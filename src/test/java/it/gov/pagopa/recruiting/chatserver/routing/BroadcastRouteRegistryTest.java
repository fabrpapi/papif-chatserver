package it.gov.pagopa.recruiting.chatserver.routing;

import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.netty.buffer.ByteBuf;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import it.gov.pagopa.recruiting.chatserver.channel.SendAtNewLineChannelHandler;

public class BroadcastRouteRegistryTest {
	
	@Test
	public void broadcastMessageFromClient1To2() {
		BroadcastRouteRegistry registry = new BroadcastRouteRegistry();
		EmbeddedChannel channel1 = new EmbeddedChannel(new StringEncoder(), new StringDecoder(), new SendAtNewLineChannelHandler(registry));
		EmbeddedChannel channel2 = new EmbeddedChannel(new StringEncoder(), new StringDecoder(), new SendAtNewLineChannelHandler(registry));
		
		channel1.writeInbound("test 1-2\n");
		Assertions.assertEquals("test 1-2\n", ((ByteBuf)channel2.readOutbound()).toString(StandardCharsets.UTF_8));

		channel2.writeInbound("test 2-1\n");
		Assertions.assertEquals("test 2-1\n", ((ByteBuf)channel1.readOutbound()).toString(StandardCharsets.UTF_8));
	}
	
	@Test
	public void notSendMessageBackToSourceClient() {
		BroadcastRouteRegistry registry = new BroadcastRouteRegistry();
		EmbeddedChannel channel1 = new EmbeddedChannel(new StringEncoder(), new StringDecoder(), new SendAtNewLineChannelHandler(registry));
		
		channel1.writeInbound("test\n");
		Assertions.assertNull(channel1.readOutbound());
	}
	
	
	@Test
	public void unsubscribeChannel() {
		BroadcastRouteRegistry registry = new BroadcastRouteRegistry();
		EmbeddedChannel channel1 = new EmbeddedChannel(new StringEncoder(), new StringDecoder(), new SendAtNewLineChannelHandler(registry));
		EmbeddedChannel channel2 = new EmbeddedChannel(new StringEncoder(), new StringDecoder(), new SendAtNewLineChannelHandler(registry));
		
		channel1.writeInbound("test 1-2\n");
		Assertions.assertEquals("test 1-2\n", ((ByteBuf)channel2.readOutbound()).toString(StandardCharsets.UTF_8));

		channel2.writeInbound("test 2-1\n");
		Assertions.assertEquals("test 2-1\n", ((ByteBuf)channel1.readOutbound()).toString(StandardCharsets.UTF_8));
		
		registry.unsubscribe(channel2);
		
		channel1.writeInbound("test 1-2\n");
		Assertions.assertNull(channel2.readOutbound());
	}
}
