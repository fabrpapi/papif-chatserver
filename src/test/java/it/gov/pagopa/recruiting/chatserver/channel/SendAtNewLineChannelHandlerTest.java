package it.gov.pagopa.recruiting.chatserver.channel;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import it.gov.pagopa.recruiting.chatserver.routing.RouteRegistry;

public class SendAtNewLineChannelHandlerTest {

	@Test
	public void sendMessageWithNewLine() {
		EmbeddedChannel channel = createSendAtNewLineChannel();
		channel.writeInbound("test\n");
		Assertions.assertEquals("test\n", ((ByteBuf)channel.readOutbound()).toString(StandardCharsets.UTF_8));
	}

	@Test
	public void sendMessageWithouthNewLine() {
		EmbeddedChannel channel = createSendAtNewLineChannel();
		channel.writeInbound("test");
		Assertions.assertNull(channel.readOutbound());
	}

	@Test
	public void properlyPropagateRouteLogicException() {
		EmbeddedChannel channel = new EmbeddedChannel(new StringEncoder(), new StringDecoder(), new SendAtNewLineChannelHandler(new RouteRegistry() {
			@Override public void unsubscribe(Channel client) {}
			@Override public void subscribe(Channel client) {}
			@Override public List<ChannelFuture> process(Channel source, String message) {
				throw new UnsupportedOperationException();
			}
		}));
		Assertions.assertThrows(UnsupportedOperationException.class, () -> {
			channel.writeInbound("test\n");
		});
	}

	private EmbeddedChannel createSendAtNewLineChannel() {
		EmbeddedChannel channel = new EmbeddedChannel(new StringEncoder(), new StringDecoder(), new SendAtNewLineChannelHandler(new RouteRegistry() {
			@Override public void unsubscribe(Channel client) {}
			@Override public void subscribe(Channel client) {}
			@Override public List<ChannelFuture> process(Channel source, String message) {
				return Arrays.asList(source.writeAndFlush(message));
			}
		}));
		return channel;
	}

}
