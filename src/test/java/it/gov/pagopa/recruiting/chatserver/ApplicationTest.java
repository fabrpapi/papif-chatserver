package it.gov.pagopa.recruiting.chatserver;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { ApplicationTestConfiguration.class })
public class ApplicationTest {
	
	@Test
	public void definitionPortFromArgument() {
		String[] args = {"8080"};
		Assertions.assertEquals(8080, Application.parsePort(args));
	}
	
	@Test
	public void wrongPortFormatFromArgument() {
		String[] args = {"a"};
		Assertions.assertEquals(Application.DEFAULT_PORT, Application.parsePort(args));
	}
	
}
