package it.gov.pagopa.recruiting.chatserver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import it.gov.pagopa.recruiting.chatserver.server.Server;
import it.gov.pagopa.recruiting.chatserver.server.ServerDefinition;

@Configuration
public class ApplicationTestConfiguration {
	
	@Bean
	public Server chatServer() throws InterruptedException {
		return new Server() {
			ServerDefinition definition;

			@Override
			public void start() {}

			public ServerDefinition getDefinition() {
				return definition;
			}

			public void setDefinition(ServerDefinition definition) {
				this.definition = definition;
			}
		
		};
	}

}
