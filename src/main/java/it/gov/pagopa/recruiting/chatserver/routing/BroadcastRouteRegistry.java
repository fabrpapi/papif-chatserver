package it.gov.pagopa.recruiting.chatserver.routing;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import lombok.extern.slf4j.Slf4j;

/**Broadcast route algorithm implementations.
 * @author Fabrizio Papi
 *
 */
@Slf4j
@Component
public class BroadcastRouteRegistry implements RouteRegistry {
	
	private final Set<Channel> clients = new HashSet<>();
	
	@Override
	public void subscribe(Channel client) {
		log.trace("subscribe(client={})", client);
		clients.add(client);
	}
	
	@Override
	public void unsubscribe(Channel client) {
		log.trace("unsubscribe(client={})", client);
		clients.remove(client);
	}
	
	@Override
	public List<ChannelFuture> process(Channel source, String message) {
		log.trace("process(source={},message={})", source, message);
		log.debug("routing the message {} to {}", message, source.remoteAddress());
		return clients.stream()
				.filter((client) -> !client.equals(source))
				.map((client) -> client.writeAndFlush(message))
				.collect(Collectors.toList());
	}

}
