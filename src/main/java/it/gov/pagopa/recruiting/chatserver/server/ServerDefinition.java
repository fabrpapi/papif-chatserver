package it.gov.pagopa.recruiting.chatserver.server;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**Basic Server options.
 * @author Fabrizio Papi
 *
 */
@Data
@NoArgsConstructor @AllArgsConstructor @Builder
public class ServerDefinition {

	private Integer port;
	
}
