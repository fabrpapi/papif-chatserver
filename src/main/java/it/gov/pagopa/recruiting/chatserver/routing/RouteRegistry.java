package it.gov.pagopa.recruiting.chatserver.routing;

import java.util.List;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;

/**Basic operations to route message from different clients. All routing algorithm must implements at least this operations to be used by chat server.
 * @author Fabrizio Papi
 *
 */
public interface RouteRegistry {
	
	void subscribe(Channel client);
	
	void unsubscribe(Channel client);
	
	List<ChannelFuture> process(Channel source, String message);

}
