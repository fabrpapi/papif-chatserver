package it.gov.pagopa.recruiting.chatserver.server;

/**Basic Server operations and configuration.
 * @author Fabrizio Papi
 *
 */
public interface Server {
	
	ServerDefinition getDefinition();
	void setDefinition(ServerDefinition def);

	void start() throws InterruptedException;
	
}
