package it.gov.pagopa.recruiting.chatserver.server;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import it.gov.pagopa.recruiting.chatserver.channel.TelnetChannelHandler;
import it.gov.pagopa.recruiting.chatserver.routing.BroadcastRouteRegistry;
import it.gov.pagopa.recruiting.chatserver.routing.RouteRegistry;

/**Telnet Chat Server configuration.
 * @author Fabrizio Papi
 *
 */
@Configuration
public class ServerConfig {
	
	@Bean
	public ChatServer chatServer(ChannelInitializer<SocketChannel> clientHandler, EventLoopGroup connectionEnstabilisher, EventLoopGroup singleConnectionHandler) {
		return new ChatServer(clientHandler, connectionEnstabilisher, singleConnectionHandler);
	}
	
	@Bean
	public ChannelInitializer<SocketChannel> clientHandler(RouteRegistry registry) {
		return new TelnetChannelHandler(registry);
	}
	
	@Bean 
	public RouteRegistry registry() {
		return new BroadcastRouteRegistry();
	}

	@Bean(destroyMethod = "shutdownGracefully")
	public EventLoopGroup connectionEnstabilisher() {
		return new NioEventLoopGroup(); 
	}
	
	@Bean(destroyMethod = "shutdownGracefully")
	public EventLoopGroup singleConnectionHandler() {
		return new NioEventLoopGroup(); 
	}
}

