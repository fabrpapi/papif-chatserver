package it.gov.pagopa.recruiting.chatserver.server;

import org.springframework.stereotype.Component;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**Write a very simple chat server that should listen on TCP port 10000 for clients. The chat protocol is very simple, clients connect with "telnet" and write single lines of text. On each new line of text, the server will broadcast that line to all other connected clients. Your program should be fully tested too. (provide a link to your solution)
 * @author Fabrizio Papi
 *
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ChatServer implements Server {

	private final ChannelInitializer<SocketChannel> clientHandler;
	private final EventLoopGroup connectionEnstabilisher;
	private final EventLoopGroup singleConnectionHandler;

	@Getter @Setter
	private ServerDefinition definition;

	@Override
	public void start() throws InterruptedException {
		log.info("starting chat server with configuration {}...", definition);
		ServerBootstrap server = new ServerBootstrap();
		server.group(connectionEnstabilisher, singleConnectionHandler)
		.channel(NioServerSocketChannel.class)
		.childHandler(clientHandler)
		.childOption(ChannelOption.SO_KEEPALIVE, true);

		if (bind(definition.getPort(), server)) {
			log.info("chat server started!");
		}
		log.info("stopping chat server");
	}

	private boolean bind(int port, ServerBootstrap b) throws InterruptedException {
		ChannelFuture startServerOp = b.bind(port).sync();
		boolean result = startServerOp.isSuccess();
		startServerOp.channel().closeFuture().sync();
		return result;
	}

}
