package it.gov.pagopa.recruiting.chatserver.channel;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import it.gov.pagopa.recruiting.chatserver.routing.RouteRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**Channel Handler implementations to buffer message until New-Line character. Can support any route algorithm implementing RouteRegistry interface.
 * @author Fabrizio Papi
 *
 */
@Slf4j
@RequiredArgsConstructor
public class SendAtNewLineChannelHandler extends SimpleChannelInboundHandler<String> {
	
	private final RouteRegistry registry;
	
	private StringBuffer message = new StringBuffer();

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        log.info("subscribing channel for address: {}", ctx.channel().remoteAddress());
        registry.subscribe(ctx.channel());
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String s) throws Exception {
        log.debug("received from {}: {}", ctx.channel().remoteAddress(), s);
        message.append(s);
        if (s.endsWith("\n")) {
        	registry.process(ctx.channel(), message.toString());
        	message.setLength(0);
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        log.info("unsubscribing channel for address: {}", ctx.channel().remoteAddress());
        registry.unsubscribe(ctx.channel());
    }
}

