package it.gov.pagopa.recruiting.chatserver.channel;

import org.springframework.stereotype.Component;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import it.gov.pagopa.recruiting.chatserver.routing.RouteRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**Telnet Channel implementations. Can support any route algorithm implementing RouteRegistry interface. Support only UTF-8 text encoding. Message will be sent at new-line character.
 * @author Fabrizio Papi
 *
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class TelnetChannelHandler extends ChannelInitializer<SocketChannel> {
	
	private final RouteRegistry registry;

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
    	log.trace("initChannel(socketChannel={})", socketChannel);
        socketChannel.pipeline().addLast(new StringEncoder());
        socketChannel.pipeline().addLast(new StringDecoder());
        socketChannel.pipeline().addLast(new SendAtNewLineChannelHandler(registry));
    }

}
