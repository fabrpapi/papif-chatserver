package it.gov.pagopa.recruiting.chatserver;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import it.gov.pagopa.recruiting.chatserver.server.ChatServer;
import it.gov.pagopa.recruiting.chatserver.server.ServerConfig;
import it.gov.pagopa.recruiting.chatserver.server.ServerDefinition;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Application  {
	static final int DEFAULT_PORT = 10000;

	public static void main(String[] args) {
		log.info("preparing the chat server to be started");
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ServerConfig.class)) {
			ServerDefinition def = ServerDefinition.builder()
				.port(parsePort(args))
				.build();
			log.info("the chat server will use the follow definition: {}", def);
			ChatServer server = context.getBean(ChatServer.class);
			server.setDefinition(def);
			server.start();
		} catch (InterruptedException e) {
			log.error(e.getMessage(), e);
		}
		log.info("the chat server has been shutted down");
	}

	/** Parse input arguments for a valid port value [1024-65535]. The port must be the first program argument. In case the port is missing or malformed, the default port will be returned.  
	 * @param args the program arguments
	 * @return the port
	 */
	static int parsePort(String[] args) {
		int port = DEFAULT_PORT;
		if(args.length >= 1) {
			try {
				int candidate = Integer.parseInt(args[0]);
				if (candidate >= 1024 && candidate <= 65535) {
					port = candidate;
				} else { 
					throw new NumberFormatException();
				}
			}catch(NumberFormatException e) {
				log.warn("{} is not a valid port (must be a number in [1024-65535] interval) , starting with default port {}.", args[0], port);
			}
		}
		return port;
	}

}
